import { Express } from "express";
import DailyTrendsController from "./DailyTrends.controller";

const URI = "/api/v1/dailytrends";

export default class {
  private readonly app: Express;
  private readonly dailyTrendsController: DailyTrendsController;

  constructor(app) {
    this.app = app;
    this.dailyTrendsController = new DailyTrendsController();
    this.configureRoutes();
  }

  private configureRoutes(): void {
    this.app.get(`${URI}/today`, this.dailyTrendsController.getTodayFeeds);
    // CRUD
    this.app.get(`${URI}/get/:id`, this.dailyTrendsController.findOne);
    this.app.get(URI, this.dailyTrendsController.findAll);
    this.app.post(URI, this.dailyTrendsController.create);
    this.app.put(URI, this.dailyTrendsController.update);
    this.app.delete(`${URI}/:id`, this.dailyTrendsController.delete);

    this.app.get(`${URI}/ping`, this.dailyTrendsController.ping);
  }
}
