import { Request, Response } from "express";
import * as i18n from "i18n";
import DailyTrendsService from "./DailyTrends.service";
import Common from "../Common";

export default class {
  private readonly dailyTrendsService: DailyTrendsService;

  constructor() {
    this.dailyTrendsService = new DailyTrendsService();
  }

  public findOne = async (req: Request, res: Response): Promise<void> => {
    try {
      const feed = await this.dailyTrendsService.findOne(req.params.id);
      if (!feed) res.status(404).send(i18n.__("404", { resource: "Feed" }));
      else res.send(feed);
    } catch (e) {
      Common.sendInternalError(e, res);
    }
  };

  public findAll = async (req: Request, res: Response): Promise<void> => {
    try {
      const feeds = await this.dailyTrendsService.findAll();
      res.send(feeds);
    } catch (e) {
      Common.sendInternalError(e, res);
    }
  };

  public create = async (req: Request, res: Response): Promise<void> => {
    try {
      const feed = await this.dailyTrendsService.create(req.body);
      res.status(201).send(feed);
    } catch (e) {
      Common.sendInternalError(e, res);
    }
  };

  public update = async (req: Request, res: Response): Promise<void> => {
    try {
      const feed = await this.dailyTrendsService.update(req.body);
      res.send(feed);
    } catch (e) {
      Common.sendInternalError(e, res);
    }
  };

  public delete = async (req: Request, res: Response): Promise<void> => {
    try {
      const feed = await this.dailyTrendsService.delete(req.params.id);
      if (!feed) res.status(404).send(i18n.__("404", { resource: "Feed" }));
      else res.send(feed);
    } catch (e) {
      Common.sendInternalError(e, res);
    }
  };

  public getTodayFeeds = async (req, res): Promise<void> => {
    try {
      const feeds = await this.dailyTrendsService.readFeeds();
      res.send(feeds);
    } catch (e) {
      console.log(e);
      Common.sendInternalError(e, res);
    }
  };

  public ping(req, res): void {
    res.send("Pong.");
  }
}
