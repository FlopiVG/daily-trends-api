import * as rp from "request-promise";
import * as $ from "cheerio";
import * as i18n from "i18n";
import FeedModel from "../Feed.model";
import Common from "../../Common";

export default class {
  private readonly feedModel: typeof FeedModel;
  private readonly url: string;

  constructor() {
    this.feedModel = FeedModel;
    this.url = "https://elpais.com/";
  }

  public async getFeeds() {
    const feeds = await this.findTodayFeeds();

    if (feeds.length) {
      return feeds;
    } else {
      const feedsScraping = await this.scrapingPage();
      this.feedModel.insertMany(feedsScraping);
      return feedsScraping;
    }
  }

  public async findTodayFeeds() {
    return await this.feedModel.find({
      createdAt: { $gte: Common.dateOfToday },
      publisher: i18n.__("El Pais"),
      image: { $exists: true }
    });
  }

  private async scrapingPage() {
    const homeFeeds = await this.getFeedsInHomePage();
    return (await Promise.all(homeFeeds.map(this.getFeedInformation))).filter(
      f => f
    );
  }

  private async getFeedsInHomePage() {
    const homePage = await rp(this.url);

    return Object.values($(".articulo", homePage));
  }

  private getFeedInformation = async el => {
    try {
      const feedUrl = $(".articulo-titulo > a", el).attr("href");
      const urlParsed = this.parseUrl(feedUrl);

      if (urlParsed) {
        const feedHtml = await rp(urlParsed);
        const image = $(".foto.centro meta[itemprop='url']", feedHtml).attr(
          "content"
        );

        if (!image) return null;

        return {
          title: $("#articulo-titulo", feedHtml).text(),
          body: $("#cuerpo_noticia", feedHtml).text(),
          image,
          source: this.url + feedUrl,
          publisher: i18n.__("El Pais")
        };
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  };

  private parseUrl(feedUrl: string): string {
    if (/^http/.test(feedUrl) || !feedUrl) return null;
    else if (/^\//.test(feedUrl)) return this.url + feedUrl.substr(1);
    else return this.url + feedUrl;
  }
}
