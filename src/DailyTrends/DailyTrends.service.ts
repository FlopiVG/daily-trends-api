import * as rp from "request-promise";
import * as $ from "cheerio";
import * as i18n from "i18n";
import FeedModel from "./Feed.model";
import ElPaisProvider from "./providers/ElPais.provider";
import Common from "../Common";

export default class {
  private readonly feedModel: typeof FeedModel;
  private readonly elPaisProvider: ElPaisProvider;

  constructor() {
    this.feedModel = FeedModel;
    this.elPaisProvider = new ElPaisProvider();
  }

  public async findOne(id) {
    return await this.feedModel.findById(id);
  }

  public async findAll() {
    return await this.feedModel.find();
  }

  public async create(feedDto) {
    const feed = new this.feedModel(feedDto);
    await feed.save();
    return feed;
  }

  public async delete(id) {
    return await this.feedModel.findByIdAndDelete(id);
  }

  public async update(feedDto) {
    return await this.feedModel.findByIdAndUpdate(
      feedDto.id,
      { $set: feedDto },
      { new: true }
    );
  }

  public async readFeeds() {
    return [
      ...(await this.elPaisProvider.getFeeds()),
      ...(await this.getUserTodayFeeds())
    ];
  }

  public async getUserTodayFeeds() {
    return await this.feedModel.find({
      image: { $exists: true },
      publisher: "User",
      createdAt: { $gte: Common.dateOfToday }
    });
  }
}
