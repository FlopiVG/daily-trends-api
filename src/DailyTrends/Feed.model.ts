import { model, Schema } from "mongoose";

const FeedSchema = new Schema(
  {
    title: String,
    body: String,
    image: String,
    source: String,
    publisher: String
  },
  { timestamps: true }
);

export default model("feed", FeedSchema);
