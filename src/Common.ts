import { Response } from "express";
import * as i18n from "i18n";

export default class {
  static sendInternalError(e: Error, res: Response) {
    res.status(500).send(i18n.__("500"));
    console.error(e.message);
  }

  static get dateOfToday() {
    const now = new Date();

    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }
}
