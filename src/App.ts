import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as morgan from "morgan";
import * as i18n from "i18n";
import * as cors from 'cors'
import DailyTrends from "./DailyTrends";

export default class {
  private app;
  private PORT: string;

  constructor() {
    this.app = express();
    this.PORT = process.env.PORT || "3000";
    this.configure();
  }

  public start(): void {
    this.app.listen(this.PORT, () =>
      console.info(`Server listen on ${this.PORT}`)
    );
  }

  private async configure(): Promise<void> {
    i18n.configure({
      locales: ["en"],
      directory: __dirname + "/locales"
    });

    this.app.use(bodyParser.json());
    this.app.use(cors())
    this.app.use(morgan("combined"));

    await mongoose.connect(
      process.env.MONGO_URI + process.env.MONGO_DB,
      { useNewUrlParser: true }
    );
    this.addModules();
  }

  private addModules(): void {
    new DailyTrends(this.app);
  }
}
